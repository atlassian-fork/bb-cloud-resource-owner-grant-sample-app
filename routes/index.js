var express = require('express');
var router = express.Router();
var config = require('../config');
var request = require('request');

var auth_grant = {
    oauth_link: "https://bitbucket.org/account/user/bitbucket-cloud-dev/api",
    response_type: "code",
    type: {
        auth_grant_type : "Resource Owner Credentials Grant",
        definition: "<p>" +
                        "The resource owner password credentials (i.e. username and password) " +
                        "can be used directly as an authorization grant to obtain an access " +
                        "token.  The credentials should only be used when there is a high " +
                        "degree of trust between the resource owner and the client (e.g. the " +
                        "client is part of the device operating system or a highly privileged " +
                        "application), and when other authorization grant types are not " +
                        "available (such as an authorization code)" +
                    "</p>" +
                    "<p>" +
                        "Useful when you have the end user’s password but you want to use a more secure end user access token instead. This method will not work when the user has two-step verification enabled" +
                    "</p>",
        diff_with_other_grants: [
            "Don't need user initiation",
            "Can be used directly to obtain an access_token",
            "Uses the user's credentials to authenticate",
            "Resource owner credentials are used at a single request and are exchanged for an access_token. Eliminating the need for the client to store the end-users credentials by exchanging a long lived access_token/refresh_token"
        ],
        document_link : "https://developer.atlassian.com/cloud/bitbucket/oauth-2/",
        rfc_link: "https://tools.ietf.org/html/draft-ietf-oauth-v2-31#section-1.3.3"
    },
    client: {
        key: config.consumerKey,
        secret: config.consumerSecret
    },

    user: {
        email_address: config.email_address,
        password: config.password
    }
};

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', auth_grant);
});

router.get(`/healthcheck`, function(req, res) {
  res.send('OK');
});

/*
 * This is where the action happens! After the user authorizes the app, this endpoint processes the access_token
 * and invokes a REST API
 */
router.post('/oauth-callback', function(req, res) {
    console.log('==================================================================');
    console.log('Start building our request to get an access_token');
    console.log('==================================================================');

    let client_id_input = req.body.client_key_input;
    let client_secret_input = req.body.client_secret_input;

    console.log(`Generate basic authentication of consumer_key:consumer_secret: ${client_id_input}:${client_secret_input}`);
    let digested = new Buffer(`${client_id_input}:${client_secret_input}`).toString('base64');

    let username = req.body.username_input;
    let password = req.body.password_input;

    let request_fallback_strategy = (options, callback) => {
        request(options, function (error, response, body) {
            let body_json = JSON.parse(body);
            if (body_json.error != undefined) {
                console.log(error);
                res.render('request', {
                    request: {
                        method: "ERROR",
                        call: ""
                    },
                    response: {
                        body: JSON.stringify(body_json, null, 2)
                    }
                });
            }

            else {
                console.log(body);
                callback(body);
            }
        });
    };

    var callback = function (body){
        let body_json = JSON.parse(body);
        let access_token = body_json.access_token;

        console.log(`Response: \n ${JSON.stringify(body_json, null, 2)}`);
        console.log('==================================================================');
        console.log(`Callback from authorization server with access_token: ${access_token} to be used for API requests`);
        console.log('==================================================================');
        console.log(`Start building our request to GET end-user's repository with access_token : ${access_token}`);
        console.log('==================================================================');

        let options = { method: 'GET',
            url: 'https://api.bitbucket.org/2.0/repositories',
            headers:
                {
                    'Cache-Control': 'no-cache',
                    Authorization: `Bearer ${access_token}` } };

        console.log(`Request options for API invocation: \n ${JSON.stringify(options, null, 2)}`);
        request_fallback_strategy(options, function(body_repo) {
                                        res.render('request', {
                                            access_token: {
                                                access_token : access_token,
                                                request: JSON.stringify(access_token_options, null, 2),
                                                response: JSON.stringify(body_json, null, 2)
                                            },

                                            repository: {
                                                request: JSON.stringify(options, null, 2),
                                                response: JSON.stringify(JSON.parse(body_repo), null, 2)
                                            },
                                            type: {
                                                auth_grant_type: auth_grant.type.auth_grant_type
                                            }
                                        });
                                    })
    };

    let access_token_options = { method: 'POST',
        url: 'https://bitbucket.org/site/oauth2/access_token',
        headers:
            { 'Content-Type': 'application/x-www-form-urlencoded',
                'Cache-Control': 'no-cache',
                Authorization: `Basic ${digested}`
            },
        form: {
            grant_type: 'password',
            username: username,
            password: password
            }
        };

    console.log(`Request\n ${JSON.stringify(access_token_options, null, 2)}`);
    request_fallback_strategy(access_token_options, callback);
});

module.exports = router;
